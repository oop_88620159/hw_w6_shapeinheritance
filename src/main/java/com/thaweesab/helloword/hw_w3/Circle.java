package com.thaweesab.helloword.hw_w3;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author acer
 */
public class Circle extends Shape {
       
    protected double pi = 22.0/7;
    
    public Circle (double width ){
        super(width,22.0/7);
        
    }
    @Override
    public double calArea(){
        return pi * (width *width) ;
    }
    
}

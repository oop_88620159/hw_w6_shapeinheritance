/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w3;

/**
 *
 * @author acer
 */
public class TestShape {
    
    public static void main(String[] args) {
       Shape shape = new Shape(10,20);
       shape.calArea();
       System.out.println(shape.calArea());
       
       
       Square square = new Square(10);
       square.calArea();
       System.out.println(square.calArea());
       
       Circle circle = new Circle(10);
       circle.calArea();
       System.out.println(circle.calArea());
       
       Rectangle rectangle = new Rectangle(5,6);
       rectangle.calArea();
       System.out.println(rectangle.calArea());
       
       Triangle triangle = new Triangle(8,6);
       triangle.calArea();
       System.out.println(triangle.calArea());
       
        System.out.println("______________TEST________________");
       Shape[] shapes = {circle,rectangle,square,triangle};
       for(int i=0; i< shapes.length; i++){
            shapes[i].calArea();
            System.out.println(shapes[i].calArea());
        }
    }
}

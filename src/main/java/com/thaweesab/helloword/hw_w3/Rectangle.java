/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thaweesab.helloword.hw_w3;

/**
 *
 * @author acer
 */
public class Rectangle extends Shape {
    public Rectangle(double width,double height){
        super(width,height);
}
    @Override
    public double calArea(){
        return height * width;
    }
  
}
